using System;

namespace Lab1
{
    public class HomeCat : DecoratorCat
    {
        protected string Name { get; set; }

        public HomeCat(Cat cat, string name) : base(cat)
        {
            Name = name;
        }
        
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"Hello I'm Mr.{Name}. I play with sunshine.");
        }
        
    }
}