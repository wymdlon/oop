namespace Lab1
{
    public abstract class DecoratorCat : Cat
    {
        private readonly Cat _cat;

        public DecoratorCat(Cat cat) : base(cat)
        {
            _cat = cat;
        }

        public override void ShowInfo()
        {
            _cat?.ShowInfo();
        }
    }
}