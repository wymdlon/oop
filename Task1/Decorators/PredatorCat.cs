using System;

namespace Lab1
{
    public class PredatorCat : DecoratorCat
    {
        public PredatorCat(Cat cat) : base(cat) { }
        
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"Hello, I'm eat Pumba now. I'm predator!!!");
        }
    }
}