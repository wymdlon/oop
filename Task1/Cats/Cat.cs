
using System;

namespace Lab1
{
    public abstract class Cat
    {
        public int Height { get; protected set; }
        public int Weigh { get; protected set; }
        public int Age { get; protected set; }
        protected string Breed { get; set; }

        public void Eat()
        {
            Console.WriteLine("I'm eating!");
        }

        public void Sleep()
        {
            
            Console.WriteLine("I'm sleeping");
        }

        public virtual void ShowInfo()
        {
            Console.WriteLine($"My height is {Height} and weigh is {Weigh} " +
                              $"and I'm {Age} years old!");
        }

        protected Cat(Cat cat)
        {
            Height = cat.Height;
            Weigh = cat.Weigh;
            Age = cat.Age;
        }

        protected Cat(int h, int w, int age)
        {
            Height = h;
            Weigh = w;
            Age = age;
        }
    }
}