using System;

namespace Lab1
{
    public class PersianCat : Cat
    {
        public PersianCat(int h, int w, int age) : base(h, w, age)
        {
            Breed = "Persian";
        }

        public override void ShowInfo()
        {
            Console.WriteLine("I'm Persian Cat.");
            base.ShowInfo();
        }
    }
}