using System;

namespace Lab1
{
    public class JungleCat : Cat
    {
        public JungleCat(int h, int w, int age) : base(h, w, age)
        {
            Breed = "Jungle";
        }

        public override void ShowInfo()
        {
            Console.WriteLine("I'm Jungle Cat.");
            base.ShowInfo();
        }
    }
}