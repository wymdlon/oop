﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat jungleCat = new JungleCat(20, 5, 3);
            jungleCat.ShowInfo();
            Console.WriteLine("------Decorate!-------");
            jungleCat = new PredatorCat(jungleCat);
            jungleCat.ShowInfo();
            
            Console.WriteLine("\n");
            Cat persianCat = new PersianCat(11, 6, 10);
            persianCat.ShowInfo();
            Console.WriteLine("------Decorate!-------");
            persianCat = new HomeCat(persianCat, "Barsic");
            persianCat.ShowInfo();
        }
    }
}
