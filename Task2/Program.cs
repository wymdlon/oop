﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            var leader = new Composite("Leader");
            
            var chef1 = new Composite("Chef 1");
            var chef2 = new Composite("Chef 2");
            var chef3 = new Composite("Chef 3");
            var chef4 = new Composite("Chef 4");
            
            var employer1 = new Employee("Employer 1");
            var employer2 = new Employee("Employer 2");
            var employer3 = new Employee("Employer 3");
            var employer4 = new Employee("Employer 4");
            var employer5 = new Employee("Employer 5");
            
            leader.Add(chef1);
            leader.Add(chef2);
            leader.Add(chef3);
            leader.Add(chef4);
            
            chef1.Add(employer1);
            chef1.Add(employer2);
            chef1.Add(employer3);
            
            chef2.Add(employer2);
            chef2.Add(employer3);
            chef2.Add(employer4);
            
            chef3.Add(employer1);
            chef3.Add(employer5);
            
            chef4.Add(employer5);
            
            leader.Display(0);
        }
    }
}
