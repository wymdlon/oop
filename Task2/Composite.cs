using System;
using System.Collections.Generic;

namespace Task2
{
    public class Composite : Workers
    {
        private List<Workers> children = new List<Workers>();

        public Composite(string name) : base(name)
        {
        }

        public void Add(Workers worker)
        {
            children.Add(worker);
        }

        public void Remove(Workers worker)
        {
            children.Remove(worker);
        }

        public override void Display(int depth)
        {
            Console.WriteLine(new string('-', depth) + name);
            
            foreach (var child in children)
            {
                child.Display(depth + 2);
            }
        }
    }
}