using System;

namespace Task2
{
    public class Employee : Workers
    {
        public Employee(string name) : base(name)
        {
        }

        public override void Display(int depth)
        {
            Console.WriteLine(new string('-', depth) + name + "");
        }
    }
}