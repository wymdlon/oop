namespace Task2
{
    public abstract class Workers
    {
        protected string name;
        
        public Workers(string name)
        {
            this.name = name;
        }

        public abstract void Display(int depth);
    }
}